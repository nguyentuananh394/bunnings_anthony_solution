FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY ["./src/DevOpsChallenge.SalesApi/DevOpsChallenge.SalesApi.csproj","./DevOpsChallenge.SalesApi/DevOpsChallenge.SalesApi.csproj"]
COPY ["./src/DevOpsChallenge.SalesApi.Business/DevOpsChallenge.SalesApi.Business.csproj","./DevOpsChallenge.SalesApi.Business/DevOpsChallenge.SalesApi.Business.csproj"]
COPY ["./src/DevOpsChallenge.SalesApi.Database/DevOpsChallenge.SalesApi.Database.csproj","./DevOpsChallenge.SalesApi.Database/DevOpsChallenge.SalesApi.Database.csproj"]

RUN dotnet restore "./DevOpsChallenge.SalesApi/DevOpsChallenge.SalesApi.csproj"
RUN dotnet restore "./DevOpsChallenge.SalesApi.Business/DevOpsChallenge.SalesApi.Business.csproj"
RUN dotnet restore "./DevOpsChallenge.SalesApi.Database/DevOpsChallenge.SalesApi.Database.csproj"

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o /publish

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build-env /publish .
ENTRYPOINT ["dotnet", "DevOpsChallenge.SalesApi.dll"] 
